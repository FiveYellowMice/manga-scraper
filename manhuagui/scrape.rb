#!/usr/bin/env ruby

require 'bundler/setup'
require 'optparse'
require 'selenium-webdriver'
require 'faraday'
require 'fileutils'
require 'pathname'

start_chapter = nil
end_chapter = nil
use_webdriver = :firefox
browser_window = false

nonopt_args = OptionParser.new do |opts|
  opts.banner = "Usage: #{$0} [options] [URL]"

  opts.on('-s', '--start CHAPTER', 'Start from a specified chapter') do |arg|
    start_chapter = arg
  end

  opts.on('-e', '--end CHAPTER', 'End at a specified chapter') do |arg|
    end_chapter = arg
  end

  opts.on('-d', '--driver DRIVER', 'Use specified WebDriver') do |arg|
    use_webdriver = arg.to_sym
  end

  opts.on('-w', '--browser-window', 'Show browser window') do
    browser_window = true
  end

  opts.on('-h', '--help', 'Show help') do
    puts opts
    exit
  end
end.parse(ARGV)

init_url = nonopt_args[0] || raise('URL not given')

if !browser_window && use_webdriver == :firefox
  drv = Selenium::WebDriver.for(:firefox, options: Selenium::WebDriver::Firefox::Options.new(args: ['-headless']))
elsif !browser_window && use_webdriver == :chrome
  drv = Selenium::WebDriver.for(:chrome, options: Selenium::WebDriver::Chrome::Options.new(args: ['--headless']))
else
  drv = Selenium::WebDriver.for(use_webdriver)
end
at_exit { drv.quit }

drv.execute_script 'window.location = "' + init_url + '"'
sleep 1
while drv.find_elements(css: '.chapter .status0').empty?
  sleep 0.1
end

book_title = drv.find_element(css: '.book-title h1').text
puts "Title: #{book_title}"

chapters = drv.find_elements(css: '.chapter h4, .chapter .status0').reduce([]) do |memo, el|
  if el.tag_name == 'h4'
    memo << { section: el.text }
  else
    memo << {
      section: memo.last[:section],
      title: el.attribute('title'),
      url: el.attribute('href'),
    }
  end
end
.select{|c| c[:title] }
.map{|c| { title: c[:section] + ' ' + c[:title], url: c[:url] } }
.reverse

chapter_range = [0, -1]

2.times do |i|
  chapter_name = [start_chapter, end_chapter][i]
  if chapter_name
    chapter_number = chapters.find_index {|c| c[:title] == chapter_name }
    if chapter_number
      chapter_range[i] = chapter_number
    else
      raise("Chapter #{chapter_name} is not found")
    end
  end
end

chapters = chapters[chapter_range[0]..chapter_range[1]]

chapters.each do |chapter|
  next if File.exist?("#{book_title}/#{chapter[:title]}")
  puts "Downloading #{chapter[:title]}..."
  FileUtils.mkdir_p "#{book_title}/#{chapter[:title]}.part"

  drv.execute_script 'window.location = "' + chapter[:url] + '"'
  sleep 1
  while drv.find_elements(id: 'mangaFile').empty?
    sleep 0.1
  end

  index = 0

  loop do
    index += 1

    img_url = drv.find_element(id: 'mangaFile').attribute('src')
    file_name = "#{book_title}/#{chapter[:title]}.part/#{index.to_s.rjust(2, '0')}#{Pathname.new(img_url.split('?')[0]).extname}"

    unless File.exist?(file_name) && File.size(file_name) > 0
      print index.to_s.rjust(2, '0') + ' '
      begin
        img_content_req = Faraday.get(img_url) do |req|
          req.headers['Referer'] = chapter[:url]
        end
        case img_content_req.status
        when 200
        when 502
          raise Faraday::ConnectionFailed, "Error downloading image: HTTP status code #{img_content_req.status}"
        else
          raise "Error downloading image: HTTP status code #{img_content_req.status}"
        end
        File.write(file_name, img_content_req.body)
      rescue Faraday::ConnectionFailed
        sleep 1
        print 'retry '
        retry
      end
    end

    drv.find_element(id: 'next').click
    break if drv.find_elements(id: 'pb').any?
  end

  File.rename("#{book_title}/#{chapter[:title]}.part", "#{book_title}/#{chapter[:title]}")
  print ?\n
end
